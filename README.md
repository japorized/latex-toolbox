# My Personal LaTeX Toolbox

This is a repo of my own toolbox for stuff related to LaTeX, like my custom
classes and styles. You are welcome to snoop around, but I won't be providing
examples.

---

### Usage

Put the files in your `$TEXMFHOME` directory. To find out where that is, run

```bash
kpsewhich --var-value=TEXMFHOME
```

The file `bootstrap` is a personal script to automate this process. Read the
script and understand it before running.

```
./bootstrap
```
