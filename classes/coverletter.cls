% Copyright note: Japorized <japorized (at) tuta (dot) io>
% I highly recommend that users use lualatex in order to use fontspec
% Also, I recommend the EB Garamond typeface, which you can get at
% https://fonts.google.com/specimen/EB+Garamond
\ProvidesClass{coverletter}[Japorized's Personal Cover Letter Header Style]
\LoadClass[12pt]{article}
\newcommand{\@covlet@pkgname}{coverletter}

\RequirePackage{geometry}
\geometry{left=1.4cm, top=.6cm, right=1.4cm, bottom=1.3cm, footskip=.5cm}
\linespread{1.3}

\RequirePackage[fixed]{fontawesome5}
\RequirePackage{hardwrap} % error logging
\GenerateClassLogMacros[@covlet]{\@covlet@pkgname}
\RequirePackage{etoolbox}
\RequirePackage{hyperref}
\hypersetup{
    unicode=true,          % non-Latin characters in Acrobat’s bookmarks
    pdffitwindow=true,     % window fit to page when opened
    colorlinks=true,
    allcolors=black,
}
\newcommand*\nbvspace[1][3]{\vspace*{\stretch{#1}}}
\newcommand*\nbstretchyspace{\spaceskip0.5em plus 0.25em minus 0.25em}
\newcommand*{\nbtitlestretch}{\spaceskip0.6em}

% personal info
\newcommand*{\credentials}[1]{\gdef\@credentials{#1}}
\newcommand*{\@credentials}{}
\newcommand*{\institution}[1]{\gdef\@institution{#1}}
\newcommand*{\@institution}{}
\newcommand*{\whoami}[1]{\gdef\@whoami{#1}}
\newcommand*{\@whoami}{}
\newcommand*{\location}[1]{\gdef\@location{#1}}
\newcommand*{\@location}{}
\newcommand*{\signature}[1]{\gdef\@signature{#1}}
\newcommand*{\@signature}{}

% Contact methods
\newcommand*{\website}[1]{\gdef\@website{#1}}
\newcommand*{\@website}{}
\newcommand*{\contactno}[1]{\gdef\@contactno{#1}}
\newcommand*{\@contactno}{}
\newcommand*{\email}[1]{\gdef\@email{#1}}
\newcommand*{\@email}{}

% Social/Dev identities
\newcommand*{\github}[1]{\gdef\@github{#1}}
\newcommand*{\@github}{}
\newcommand*{\gitlab}[1]{\gdef\@gitlab{#1}}
\newcommand*{\@gitlab}{}
\newcommand*{\linkedin}[1]{\gdef\@linkedin{#1}}
\newcommand*{\@linkedin}{}
\newcommand*{\twitter}[1]{\gdef\@twitter{#1}}
\newcommand*{\@twitter}{}

% Salutations
\newcommand*{\salutation}[1]{\gdef\@salutation{#1}}
\newcommand*{\@salutation}{Greetings}
\newcommand*{\letterending}[1]{\gdef\@letterending{#1}}
\newcommand*{\@letterending}{Best regards}

% Macros
\newcommand*{\social}[3]{\href{#2/#1}{#3\ #2/#1}} % 1 - item; 2 - url sans identifier; 3 - FA icon
\newcommand*{\Email}{\ifdefempty{\@email}{\@covlet@error{Email not provided}{Please provide email address}}{\href{mailto:\@email}{\faEnvelope\ \@email}\ \ |}}
\newcommand*{\Contact}{\ifdefempty{\@contactno}{}{\faPhone\ \@contactno\ \ |}}
\newcommand*{\Website}{\ifdefempty{\@website}{}{\href{\@website}{\faLink\ \@website}\ \ |}}
\newcommand*{\Location}{\ifdefempty{\@location}{}{\faLocationArrow\ \@location}}
\newcommand*{\Github}{\ifdefempty{\@github}{}{\social{\@github}{https://github.com}{\faGithub}\ \ }}
\newcommand*{\Gitlab}{\ifdefempty{\@gitlab}{}{\social{\@gitlab}{https://gitlab.com}{\faGitlab}\ \ }}
\newcommand*{\Linkedin}{\ifdefempty{\@linkedin}{}{\social{\@linkedin}{https://www.linkedin.com/in}{\faLinkedin}\ \ }}
\newcommand*{\Twitter}{\ifdefempty{\@twitter}{}{\social{\@twitter}{https://twitter.com}{\faTwitter}\ \ }}

% Title and signature generator
\renewcommand*{\maketitle}{%
\begin{center}
  {\Large\itshape\@author\unskip\strut\par}
  {\small\@credentials\unskip\strut\par}
  \footnotesize
  \ifdefempty{\@whoami}{}{\@whoami\unskip\strut\par}
  \ifdefempty{\@institution}{}{\@institution\unskip\strut\par}
  \Email \Contact \Website \Location \unskip\strut\par
  \Github \Gitlab \Linkedin \unskip\strut\par
  \Twitter
\end{center}
\hrule
\vspace{20px}
{\hfill \today.}
\vspace{10px}

\ifdefempty{\@title}{}{
\noindent
\textbf{\@title}\unskip\strut\par
}
\noindent
\@salutation,
\setlength{\parskip}{5px}
}

\newcommand*{\makesignature}[1][]{%
\setlength{\parskip}{0px}
\vspace{20px}
{\parindent0pt
\@letterending,\unskip\strut\par
\textbf{\@signature}\unskip\strut\par
#1
}
}
