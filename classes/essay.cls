% Copyright note: Japorized <japorized (at) tuta (dot) io>
% I highly recommend that users use lualatex in order to use fontspec
% Also, I recommend the EB Garamond typeface, which you can get at
% https://fonts.google.com/specimen/EB+Garamond
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{essay}[Japorized's Personal Essay Document Class]
\LoadClass{article}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

\newcommand{\@covlet@pkgname}{essay}

\RequirePackage{geometry}
\geometry{left=1.4cm, top=.6cm, right=1.4cm, bottom=1.3cm, footskip=.5cm}
\linespread{1.3}

\RequirePackage{hardwrap} % error logging
\GenerateClassLogMacros[@covlet]{\@covlet@pkgname}
\RequirePackage{etoolbox}

\newcommand*\nbvspace[1][3]{\vspace*{\stretch{#1}}}
\newcommand*\nbstretchyspace{\spaceskip0.5em plus 0.25em minus 0.25em}
\newcommand*{\nbtitlestretch}{\spaceskip0.6em}

% Title and signature generator
\renewcommand*{\maketitle}{%
\begin{titlepage}
  {\LARGE\@title\unskip\strut\par}
  {\large\itshape\@author\unskip\strut\par}
  \footnotesize
\end{titlepage}
}
